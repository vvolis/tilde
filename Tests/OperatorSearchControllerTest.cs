﻿using System.Collections.Generic;
using Tilde.Controllers;
using Xunit;

namespace Tests
{
    public class OperatorSearchControllerTest
    {
        [Fact]
        public void TestResult()
        {
            OperatorSearchController controller = new OperatorSearchController();
            OperatorSearchRequest request = new OperatorSearchRequest();
            request.variables = new List<int>() { 17, 3, 2, 5 };
            request.result = 23;


            OperatorSearchResponse response = controller.Search(request);

            Assert.True(response.operators == "+-+");
            Assert.True(response.success == true);
        }

        [Fact]
        public void TestNotEnoughVariables()
        {
            OperatorSearchController controller = new OperatorSearchController();
            OperatorSearchRequest request = new OperatorSearchRequest();
            request.variables = new List<int>() { 17 };
            request.result = 23;

            OperatorSearchResponse response = controller.Search(request);

            Assert.True(response.success == false);
        }

        [Fact]
        public void TestImpossibleSolution()
        {
            OperatorSearchController controller = new OperatorSearchController();
            OperatorSearchRequest request = new OperatorSearchRequest();
            request.variables = new List<int>() { 17, 3, 2, 5 };
            request.result = 48; ;

            OperatorSearchResponse response = controller.Search(request);

            Assert.True(response.success == false);
        }
    }
}
