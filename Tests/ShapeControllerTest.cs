using System.Collections.Generic;
using Tilde.Controllers;
using Xunit;

namespace Tests
{
    public class ShapeControllerTest
    {
        [Fact]
        public void Test1()
        {
            ShapeController controller = new ShapeController();
            ShapeClockOrderRequest request = new ShapeClockOrderRequest();
            request.vertices = new List<List<int>>();
            request.vertices.Add(new List<int>() { 5, 10 });
            request.vertices.Add(new List<int>() { 15, 20 });
            request.vertices.Add(new List<int>() { 20, 7 });

            ShapeClockOrderResponse response = controller.IsClockwise(request);
            Assert.True(response.clockwise == true);
            Assert.True(response.success == true);
        }

        [Fact]
        public void Test2()
        {
            ShapeController controller = new ShapeController();
            ShapeClockOrderRequest request = new ShapeClockOrderRequest();
            request.vertices = new List<List<int>>();
            request.vertices.Add(new List<int>() { 5, 10 });
            request.vertices.Add(new List<int>() { 20, 7 });
            request.vertices.Add(new List<int>() { 15, 20 });

            ShapeClockOrderResponse response = controller.IsClockwise(request);
            Assert.True(response.clockwise == false);
            Assert.True(response.success == true);
        }

        [Fact]
        public void Test3()
        {
            ShapeController controller = new ShapeController();
            ShapeClockOrderRequest request = new ShapeClockOrderRequest();
            request.vertices = new List<List<int>>();
            request.vertices.Add(new List<int>() { 4, 3 });
            request.vertices.Add(new List<int>() { 3, 3 });
            request.vertices.Add(new List<int>() { 3, 5 });
            request.vertices.Add(new List<int>() { 6, 5 });
            request.vertices.Add(new List<int>() { 6, 3 });
            request.vertices.Add(new List<int>() { 5, 3 });
            request.vertices.Add(new List<int>() { 5, 2 });
            request.vertices.Add(new List<int>() { 7, 2 });
            request.vertices.Add(new List<int>() { 7, 6 });
            request.vertices.Add(new List<int>() { 2, 6 });
            request.vertices.Add(new List<int>() { 2, 2 });
            request.vertices.Add(new List<int>() { 4, 2 });


            ShapeClockOrderResponse response = controller.IsClockwise(request);
            Assert.True(response.clockwise == false);
            Assert.True(response.success == true);
        }

        [Fact]
        public void TestNotEnoughVertices()
        {
            ShapeController controller = new ShapeController();
            ShapeClockOrderRequest request = new ShapeClockOrderRequest();
            request.vertices = new List<List<int>>();
            request.vertices.Add(new List<int>() { 4, 3 });

            ShapeClockOrderResponse response = controller.IsClockwise(request);
            Assert.True(response.success == false);
        }

        [Fact]
        public void TestIncorrectCorrdinates()
        {
            ShapeController controller = new ShapeController();
            ShapeClockOrderRequest request = new ShapeClockOrderRequest();
            request.vertices = new List<List<int>>();
            request.vertices.Add(new List<int>() { 4 });
            request.vertices.Add(new List<int>() { 4, 3 });
            request.vertices.Add(new List<int>() { 4, 3, 2 });

            ShapeClockOrderResponse response = controller.IsClockwise(request);
            Assert.True(response.success == false);
        }
    }
}
