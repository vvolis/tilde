﻿using Microsoft.EntityFrameworkCore;
using Tilde.Controllers;
using Tilde.Models;
using Xunit;
using static Tilde.Controllers.LoanController;

namespace Tests
{
    public class LoanControllerTest
    {
        LoanContext Context;
        public LoanControllerTest()
        {
            var options = new DbContextOptionsBuilder<LoanContext>()
              .UseInMemoryDatabase(databaseName: "tests")
              .Options;

            Context = new LoanContext(options);
        }

        public void Dispose()
        {
            //Clean after yourself
            Context.Users.RemoveRange(Context.Users);
            Context.SaveChanges();
            Context.Dispose();
        }

        [Fact]
        public void CantRegisterTwice()
        {
            LoanController controller = new LoanController(Context);
            RegisterUserRequest request = new RegisterUserRequest() { name = "testuser" };
            RegisterUserResponse response = controller.Register(request);
            response = controller.Register(request);

            Assert.True(response.userId == null);
            Assert.True(response.success == false);
        }

        [Fact]
        public void TestEntireFlow()
        {
            LoanController controller = new LoanController(Context);


            RegisterUserRequest usrRequest = new RegisterUserRequest() { name = "testuser-full-1" };
            RegisterUserResponse usrResponse1 = controller.Register(usrRequest);
            Assert.True(usrResponse1.success == true);

            usrRequest = new RegisterUserRequest() { name = "testuser-full-2" };
            RegisterUserResponse usrResponse2 = controller.Register(usrRequest);
            Assert.True(usrResponse2.success == true);


            LoanRequest request = new LoanRequest() { lenderId = (int)usrResponse1.userId, loanerId = (int)usrResponse2.userId, amount = 50.0f };
            LoanResponse response = controller.DoLoan(request);
            Assert.True(response.success == true);

            PaybackRequest pbRequest = new PaybackRequest() { loanId = response.loanId, amount = 20.0f };
            PaybackResponse pbResponse = controller.Payback(pbRequest);
            Assert.True(pbResponse.success == true);

            pbRequest = new PaybackRequest() { loanId = response.loanId, amount = 15.0f };
            pbResponse = controller.Payback(pbRequest);
            Assert.True(pbResponse.success == true);


            InfoResponse inforResponse = controller.Info((int)usrResponse1.userId);
            Assert.True(inforResponse.success == true);
            Assert.True(inforResponse.lended == 15.0f);
            Assert.True(inforResponse.loaned == 0.0f);
            Assert.True(inforResponse.positiveBilance == true);
            Assert.True(inforResponse.success == true);

            InfoResponse inforResponse2 = controller.Info((int)usrResponse2.userId);
            Assert.True(inforResponse2.success == true);
            Assert.True(inforResponse2.lended == 0.0f);
            Assert.True(inforResponse2.loaned == 15.0f);
            Assert.True(inforResponse2.positiveBilance == false);
            Assert.True(inforResponse2.success == true);

        }


    }
}
