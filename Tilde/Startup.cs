using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Tilde.Models;

namespace Tilde
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContextPool<LoanContext>(options  => options.UseSqlite("Data Source=loans.db"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            SeedDefaultData();
        }

        void SeedDefaultData()
        {
            var options = new DbContextOptionsBuilder<LoanContext>()
              .UseSqlite("Data Source=loans.db")
              .Options;

            var context = new LoanContext(options);

            if (context.Users.ToList().Count == 0) {
                User usr1 = new User() { Name = "User1" };
                User usr2 = new User() { Name = "User2" };
                User usr3 = new User() { Name = "User3" };
                User usr4 = new User() { Name = "User4" };
                User usr5 = new User() { Name = "User5" };




                Loan loan1 = new Loan() { Loaner = usr1, Lender = usr2, Amount = 5.0f };
                for (int i = 0; i < 5; ++i) {
                    Payment payment = new Payment() { Loan = loan1, Amount = 1.0f };
                    context.Payments.Add(payment);
                }
                context.Loans.Add(loan1);


                Loan loan2 = new Loan() { Loaner = usr2, Lender = usr3, Amount = 5757.0f };
                for (int i = 0; i < 5; ++i) {
                    Payment payment = new Payment() { Loan = loan2, Amount = 17.0f };
                    context.Payments.Add(payment);
                }
                context.Loans.Add(loan2);

                Loan loan3 = new Loan() { Loaner = usr3, Lender = usr4, Amount = 400.0f };
                for (int i = 0; i < 5; ++i) {
                    Payment payment = new Payment() { Loan = loan3, Amount = 4.0f };
                    context.Payments.Add(payment);
                }
                context.Loans.Add(loan3);

                Loan loan4 = new Loan() { Loaner = usr4, Lender = usr5, Amount = 800.0f };
                for (int i = 0; i < 5; ++i) {
                    Payment payment = new Payment() { Loan = loan4, Amount = 6.0f };
                    context.Payments.Add(payment);
                }
                context.Loans.Add(loan4);




                context.Users.Add(usr1);
                context.Users.Add(usr2);
                context.Users.Add(usr3);
                context.Users.Add(usr4);
                context.Users.Add(usr5);

                context.SaveChanges();
            }



        }
    }
}
