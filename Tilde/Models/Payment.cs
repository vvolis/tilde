﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tilde.Models
{
    public class Payment
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        public Loan Loan { get; set; }
        public float Amount { get; set; }
    }
}
