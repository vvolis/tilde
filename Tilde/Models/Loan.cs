﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Tilde.Models
{
    public class Loan
    {
        public Loan()
        {
            Payments = new List<Payment>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        public User Lender { get; set; }
        public User Loaner { get; set; }
        public float Amount { get; set; }
        public List<Payment> Payments { get; set; }

        public float GetRemaining()
        {
            return Amount - Payments.Sum(payment => payment.Amount);
        }
}
}
