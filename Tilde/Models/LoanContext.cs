﻿using Microsoft.EntityFrameworkCore;

namespace Tilde.Models
{

    public class LoanContext : DbContext
    {
        public LoanContext(DbContextOptions<LoanContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Loan> Loans { get; set; }

        public DbSet<Payment> Payments { get; set; }

       /* protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=loans.db");*/

    }
}
