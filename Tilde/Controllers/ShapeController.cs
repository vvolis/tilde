﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tilde.Controllers
{

    [JsonObject]
    public class ShapeClockOrderRequest
    {
        [Required]
        [JsonProperty("vertices"), JsonRequired]
        public List<List<int>> vertices { get; set; }
    }

    [JsonObject]
    public class ShapeClockOrderResponse
    {
        [JsonProperty("success")]
        public bool success { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("clockwise")]
        public bool? clockwise { get; set; }

    }


    [Route("shape")]
    [ApiController]
    public class ShapeController : ControllerBase
    {
        /*
         * Solution of the problem implemented by using
         * shoelace algorithm: https://en.wikipedia.org/wiki/Shoelace_formula
         * We do not actually care for the area of the shape
         * but the specific of this algorithm guarantees that the output area is 
         * negative when going clockwise and positive when going counterclockwise
         */

        [HttpPost("is-clockwise")]
        public ShapeClockOrderResponse IsClockwise(ShapeClockOrderRequest request)
        {
            ShapeClockOrderResponse response = new ShapeClockOrderResponse() { success = true };

            if (request.vertices.Count < 3) {
                response.message = "Error: Not enough vertices found. At least 3 required";
                response.success = false;
                return response;
            }

            foreach (var vertex in request.vertices) {
                if (vertex.Count != 2) {
                    response.success = false;
                    response.message = "Error: All vertices require exactly 2 coordinate points defined";
                    return response;
                }
            }


            //Shoelace algorithm
            var area = 0;
            for (var i = 0; i < request.vertices.Count; i++) {
                var j = (i + 1) % request.vertices.Count;
                area += request.vertices[i][0] * request.vertices[j][1];
                area -= request.vertices[j][0] * request.vertices[i][1];
            }

            //Detect clockwiseness by the sign of the area
            response.clockwise = (area < 0);
            return response;
        }

    }
}
