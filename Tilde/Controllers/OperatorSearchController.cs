﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tilde.Controllers
{
    [JsonObject]
    public class OperatorSearchRequest
    {
        [Required]
        [JsonProperty("variables"), JsonRequired]
        public List<int> variables { get; set; }

        [Required]
        [JsonProperty("result"), JsonRequired]
        public int? result { get; set; }
    }

    [JsonObject]
    public class OperatorSearchResponse
    {
        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("operators")]
        public string operators { get; set; }

        [JsonProperty("operators")]
        public bool success { get; set; }

    }


    [Route("operators")]
    [ApiController]
    public class OperatorSearchController : Controller
    {

        /*
         * The solution of the problem implemented by sheer brute force
         * At first we generate all the possible combinations of "+"and "-" operators in the required amount (variable count -1)
         * Then loop throguh each of them and find one that fits the required result.
         * If none found, it will return fail message to the user.
         * The complexity of the algorithm is O(n^2)
        */


        [HttpPost("search")]
        public OperatorSearchResponse Search(OperatorSearchRequest request)
        {
            OperatorSearchResponse response = new OperatorSearchResponse() { success = true };
            if (request.variables.Count < 2 || request.result == null) {
                response.message = "At least 2 variables and result required";
                response.success = false;
                return response;
            }

            //Generate all possible combinations of the operators
            List<string> operators = new List<string>() { "+", "-" };
            List<string> possibilities = getCombinations("", operators, request.variables.Count - 1);

            foreach (var possibility in possibilities) {

                int sum = request.variables[0];

                //For the current possible solution loop through all variables and do the chosen arithmetic
                //operation on it
                for (int i = 1; i < request.variables.Count; i++) {
                    var operation = possibility[i - 1];
                    if (operation == '+') {
                        sum += request.variables[i];
                    } else {
                        sum -= request.variables[i];
                    }
                }

                //If there is at least one solution that fits, return it!
                //There might be another one later, but we dont care for that
                if (sum == request.result) {
                    response.operators = possibility;
                    return response;
                }
            }

            response.message = "No solution found";
            response.success = false;
            return response;
        }


        /*
         * Recursively keep adding options to the output
         * until we have reached depth (length of string) that fits our needs
         * Then return the result
         */
        private List<string> getCombinations(string input, List<string> options, int depth)
        {
            List<string> res = new List<string>();

            if (input.Length == depth) {
                return new List<string> { input };
            } else {

                foreach (var option in options) {
                    string newInput = input + option;
                    res.AddRange(getCombinations(newInput, options, depth));
                }
            }

            return res;
        }
    }
}
