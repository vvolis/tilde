﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Tilde.Models;

namespace Tilde.Controllers
{

    [Route("loan")]
    [ApiController]
    public class LoanController : Controller
    {

        LoanContext Context;

        public LoanController(LoanContext _Context)
        {
            Context = _Context;
        }


        [JsonObject]
        public class RegisterUserRequest
        {
            [Required]
            [JsonProperty("name"), JsonRequired]
            public string name { get; set; }
        }

        [JsonObject]
        public class RegisterUserResponse
        {
            [JsonProperty("success")]
            public bool success { get; set; }

            [JsonProperty("message")]
            public string message { get; set; }

            [JsonProperty("user-id")]
            public int? userId { get; set; }
        }


        /*
         * Very simple register function - check user by name
         * If there is none found with that name, create it and return the id
         */
        [HttpPost("register")]
        public RegisterUserResponse Register(RegisterUserRequest request)
        {
            RegisterUserResponse response = new RegisterUserResponse() { success = true };

            string userName = request.name;
            User user = Context.Users.Where(usr => usr.Name == userName).FirstOrDefault();
            if (user == null) {
                user = new User() { Name = userName };
                Context.Users.Add(user);
                Context.SaveChanges();
                response.message = "User created";
                response.userId = user.Id;
            } else {
                response.message = "User already registered";
                response.success = false;
            }

            return response;
        }






        [JsonObject]
        public class LoanRequest
        {

            [JsonProperty("lender-id")]
            public int lenderId { get; set; }
            [JsonProperty("loaner-id")]
            public int loanerId { get; set; }
            [JsonProperty("amount")]
            public float amount { get; set; }
        }

        [JsonObject]
        public class LoanResponse
        {
            [JsonProperty("message")]
            public string message { get; set; }

            [JsonProperty("success")]
            public bool success { get; set; }

            [JsonProperty("loan-id")]
            public int loanId { get; set; }
        }


        /*
         * Upon loaning we create a "Loan" object that holds ids of the both
         * parties, and the amount loaned.
         */

        [HttpPost("loan")]
        public LoanResponse DoLoan(LoanRequest request)
        {
            LoanResponse response = new LoanResponse() { success = true, message = "" };

            User lender = Context.Users.Where(usr => usr.Id == request.lenderId).FirstOrDefault();
            User loaner = Context.Users.Where(usr => usr.Id == request.loanerId).FirstOrDefault();

            if (lender == null) {
                response.message += "Lender with id " + request.lenderId + " not found";
            }

            if (loaner == null) {
                response.message += "Loaner with id " + request.loanerId + " not found";
            }

            if (response.message != "") {
                response.success = false;
                return response;
            }

            Loan loan = new Loan() { Lender = lender, Loaner = loaner, Amount = request.amount };

            Context.Loans.Add(loan);
            Context.SaveChanges();

            response.loanId = loan.Id;

            return response;
        }





        [JsonObject]
        public class PaybackRequest
        {
            [JsonProperty("loan-id")]
            public int loanId { get; set; }
            [JsonProperty("amount")]
            public float amount { get; set; }
        }

        [JsonObject]
        public class PaybackResponse
        {
            [JsonProperty("message")]
            public string message { get; set; }

            [JsonProperty("success")]
            public bool success { get; set; }
        }


        /*
         * Paying back creates a "Payment" object for the specific "Loan" object
         * with the amount paid back and nothing else.
         * we assume we don't care who actually is paying the loan back.
         */

        [HttpPost("payback")]
        public PaybackResponse Payback(PaybackRequest request)
        {
            PaybackResponse response = new PaybackResponse() { success = true };

            Loan loan = Context.Loans.Where(loan => loan.Id == request.loanId).FirstOrDefault();
            if (loan == null) {
                response.message += "ERR: Invalid loan id";
                response.success = false;
            }

            if (request.amount < 0) {
                response.message += "ERR: Cant do negative  payback";
                response.success = false;
            }

            if (response.success) {
                Payment payment = new Payment() { Loan = loan, Amount = request.amount };
                loan.Payments.Add(payment);
                Context.SaveChanges();
            }


            return response;
        }





        [JsonObject]
        public class InfoResponse
        {
            [JsonProperty("message")]
            public string message { get; set; }

            [JsonProperty("success")]
            public bool success { get; set; }

            [JsonProperty("loaned")]
            public float? loaned { get; set; }

            [JsonProperty("lended")]
            public float? lended { get; set; }

            [JsonProperty("positive-bilance")]
            public bool? positiveBilance { get; set; }

        }

        /*
         * Getting info now is quite easy
         * We get all Loan objects relating to user and calculate its corresponding balance
         * (Simply loan amount - sum of all payments back)
         * The bilance is simply the sign ofthe difference between loaned and lended amount
         */

        [HttpGet("info/{userId}")]
        public InfoResponse Info(int userId)
        {
            InfoResponse response = new InfoResponse() { success = true };

            User user = Context.Users.Where(usr => usr.Id == userId).FirstOrDefault();
            if (user == null) {
                response.success = false;
                response.message = "No such user";
            }

            List<Loan> loansLending = Context.Loans
                .Where(loan => loan.Lender == user)
                .Include(loan => loan.Payments)
                .ToList();

            List<Loan> loansLoaning = Context.Loans
               .Where(loan => loan.Loaner == user)
               .Include(loan => loan.Payments)
               .ToList();

            response.loaned = loansLoaning.Sum(loan => loan.GetRemaining());
            response.lended = loansLending.Sum(loan => loan.GetRemaining());
            response.positiveBilance = !(response.lended < response.loaned);

            return response;
        }

    }
}
