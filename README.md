## Ievads
Visi uzdevumi risināti izmantojot .NET Core serveri, katram no uzdevumiem piešķirts savs kontrolieris.
Vienkāršības labad, DB risinājumam izmantots SQLite, kura datubāzes fails ir atrodams repozitorijā.
Palaišanai var izmantot Microsoft Visual Studio 2019, atveros solution failam, IDEei vajadzētu tikt galā ar pārējo pašai.
Testu palaišana zem Test->Run All Tests
## Uzdevumu risinājumi
Uzdevums #1: https://gitlab.com/vvolis/tilde/-/blob/master/Tilde/Controllers/LoanController.cs

DB sākotnējo datu seedošana (automātiska): https://gitlab.com/vvolis/tilde/-/blob/master/Tilde/Startup.cs#L55

ER diagramma: https://dbdiagram.io/d/5f7979d03a78976d7b763f9d

DB kopija: https://gitlab.com/vvolis/tilde/-/blob/master/Tilde/loans.db


Uzdevums #2:https://gitlab.com/vvolis/tilde/-/blob/master/Tilde/Controllers/ShapeController.cs

Uzdevums #3:https://gitlab.com/vvolis/tilde/-/blob/master/Tilde/Controllers/OperatorSearchController.cs

Testi: https://gitlab.com/vvolis/tilde/-/tree/master/Tests


## API documentation
### Calculating whether shape is drawn clockwise
#### Request
```http
POST /shape/is-clockwise
```
```javascript
{
  "vertices" : [[]], //Required. List of vertices of the shape, e.g. [[2,1],[3,4],[5.6] |
}
```
#### Response
```javascript
{
  "success"     : bool, //Success flag
  "message"     : string, //Info about failure
  "clockwise"   : bool // result indicating whether shape was drawn clockwise
}
```



### Finding arithmetic operators
#### Request
```http
POST /operators/search
```
```javascript
{
  "variables"   : [], //Required. List of variables, e.g. [17, 3, 2, 5]
  "result"      : int, //Required. Expected result
}
```
#### Response
```javascript
{
  "success"     : bool, //Success flag
  "message"     : string, //Info about failure
  "operators"   : string // resulting operator list e.g. "+-+"
}
```

### Doing loans
#### --Registering
#### Request
```http
POST /loan/register
```
```javascript
{
  "name"   : string //Name of the user
}
```
#### Response
```javascript
{
  "success"     : bool, //Success flag
  "message"     : string, //Info about failure
  "user-id"   : int //Id of the created user
}
```

#### --Loaning
#### Request
```http
POST /loan/loan
```
```javascript
{
  "lender-id"   : int //Id of the lending user
  "loaner-id"   : int //Id of the loaning user
  "amount"   : float //Amount loaned
}
```
#### Response
```javascript
{
  "success"     : bool, //Success flag
  "message"     : string, //Info about failure
  "loan-id"   : int //Id of the created loan object
}
```


#### --Payback
#### Request
```http
POST /loan/payback
```
```javascript
{
  "loan-id"   : int //Id of the loan object
  "amount"   : float //Amount paid back
}
```
#### Response
```javascript
{
  "success"     : bool, //Success flag
  "message"     : string, //Info about failure
}
```


#### --Info
#### Request
```http
GET /loan/info/{userId}
```
#### Response
```javascript
{
  "success"     : bool, //Success flag
  "message"     : string, //Info about failure
  "loaned"     : float, //Amount of loaned money
  "lended"     : float, //Amount of lended money
  "positive-bilance"     : bool, //Positive bilance indicator
}
```








